'use strict';

function isLoaded() {
    console.log('Code JS chargé');
    var maBalise = document.getElementById('status');
    maBalise.innerText = "JS Chargé";
    maBalise.style.backgroundColor = 'GREEN';

    addEventsForNoteForm();
}
isLoaded();

function addEventsForNoteForm() {
    console.log('add events for note forms');

    var button = document.querySelector('button#saveNote');
    button.addEventListener('click', saveNote);

    button = document.querySelector('button#resetNote');
    button.addEventListener('click', resetNote);


}


// Gestion du formulaire de note
function saveNote() {
    console.log('save note by event');
}

function resetNote() {
    document.forms.newNote.reset();

    console.log('vidange note by event');
}